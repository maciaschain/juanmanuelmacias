#+TITLE: Entradas con la etiqueta: "poemas"

#+SETUPFILE: ~/Git/juanmanuelmacias/html-diosas.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
#+AUTHOR: Juan Manuel Macías
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+OPTIONS: ':t
#+OPTIONS: todo:nil
#+LANGUAGE: es
- [[file:dn_deber.org][Deber]]  -- 22/06/20

- [[file:dn_poema_danza.org][El poema como la danza]]  -- 21/06/20

- [[file:dn_ida_regreso.org][Ida y regreso]]  -- 21/06/20

- [[file:dn_intermitencia.org][Intermitencia]]  -- 21/06/20

- [[file:dn_katerina_anghelaki.org][Tres poemas de Katerina Anghelaki Rouke]]  -- 20/11/20
