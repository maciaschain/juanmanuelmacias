#+TITLE: Entradas con la etiqueta: "libros"

#+SETUPFILE: ~/Git/juanmanuelmacias/html-diosas.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
#+AUTHOR: Juan Manuel Macías
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+OPTIONS: ':t
#+OPTIONS: todo:nil
#+LANGUAGE: es
- [[file:dn_dunsany.org][El soñador]]  -- 22/06/20

- [[file:dn_spoilers.org][Spoilers]]  -- 03/08/21

- [[file:dn_matices_libro.org][Tres matices al libro]]  -- 21/06/20

- [[file:dn_katerina_anghelaki.org][Tres poemas de Katerina Anghelaki Rouke]]  -- 20/11/20

- [[file:dn_amanece_cantor.org][Un apunte sobre /No amanece el cantor/]]  -- 23/09/20
