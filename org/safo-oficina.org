#+TITLE: /Poesías, Safo/ (La Oficina de Arte y Ediciones)
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE:~/Git/gnutas/macros-gnutas.setup
#+SETUPFILE:~/Git/juanmanuelmacias/html.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+OPTIONS: ':t
#+OPTIONS: todo:nil
#+LANGUAGE: es


#+ATTR_HTML: :style width:30%
[[./images/Safo_portada_blog.jpg]]


*  Menciones, reseñas y ecos

- [[https://www.diariodesevilla.es/delibros/ama_0_1213378721.html][Reseña de M. Ángeles Robles en /Diario de Sevilla/]]
- [[https://www.infolibre.es/noticias/los_diablos_azules/2017/10/20/safo_poesias_70887_1821.html][Reseña de Mònica Vidiella en /Los diablos azules (Info Libre)/]]
- [[https://mayora.blogspot.com.es/2017/07/los-griegos.html][En el blog de Álvaro Valverde]]
- [[http://articulosdearturotendero.blogspot.com.es/2017/08/safo-poesias.html][Reseña de Arturo Tendero en /La tribuna/]]
- [[http://www.elcultural.com/blogs/rima-interna/2017/07/safo-cuando-la-alegre-fiesta/][Reseña de Martín López-Vega en /El cultural/]]
- [[http://poesiaintemperie.blogspot.com.es/2017/06/el-tiempo-como-artista-proposito-de-safo.html][Texto de José Luis Gómez Toré para la presentación de las /Poesías/ de Safo en Madrid]]
