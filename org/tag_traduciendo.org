#+TITLE: Entradas con la etiqueta: "traduciendo"

#+SETUPFILE: ~/Git/juanmanuelmacias/html-diosas.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
#+AUTHOR: Juan Manuel Macías
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+OPTIONS: ':t
#+OPTIONS: todo:nil
#+LANGUAGE: es
- [[file:dn_ida_regreso.org][Ida y regreso]] -- 21/06/20

- [[file:dn_polifemo.org][Polifemo, el misántropo]]  -- 24/08/23

- [[file:dn_katerina_anghelaki.org][Tres poemas de Katerina Anghelaki Rouke]]  -- 20/11/20
