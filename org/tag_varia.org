#+TITLE: Entradas con la etiqueta: "varia"

#+SETUPFILE: ~/Git/juanmanuelmacias/html-diosas.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
#+AUTHOR: Juan Manuel Macías
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+OPTIONS: ':t
#+OPTIONS: todo:nil
#+LANGUAGE: es
- [[file:dn_telemaco.org][Breve elogio de Telémaco]]  -- 10/08/20

- [[file:dn_con_sin.org][Con y/o sin]]  -- 21/06/20

- [[file:dn_creatividad.org][Creatividad]]  -- 21/06/20

- [[file:dn_deber.org][Deber]]  -- 22/06/20

- [[file:dn_poema_danza.org][El poema como la danza]]  -- 21/06/20

- [[file:dn_ida_regreso.org][Ida y regreso]]  -- 21/06/20

- [[file:dn_intermitencia.org][Intermitencia]]  -- 21/06/20

- [[file:dn_humanidades_agonias.org][Las Humanidades y sus agonías]]  -- 18/09/20

- [[file:dn_polifemo.org][Polifemo, el misántropo]]  -- 24/08/23

- [[file:dn_risa.org][Reír]]  -- 27/10/20

- [[file:dn_spoilers.org][Spoilers]]  -- 03/08/21

- [[file:dn_stallman.org][Stallman y las hordas]]  -- 29/03/21

- [[file:dn_matices_libro.org][Tres matices al libro]]  -- 21/06/20

- [[file:dn_prevenciones.org][Tres prevenciones]]  -- 02/09/21

- [[file:dn_amanece_cantor.org][Un apunte sobre /No amanece el cantor/]]  -- 23/09/20

- [[file:dn_vasilevo.org][/Vasilévo/]]  -- 19/08/20
