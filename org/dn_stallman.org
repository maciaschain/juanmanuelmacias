#+TITLE: Stallman y las hordas
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE: ~/Git/juanmanuelmacias/html-diosas.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
#+STARTUP: inlineimages
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
#+FILETAGS: :varia:

#+BIND: org-export-filter-final-output-functions (filtro-html-misc)
#+begin_src emacs-lisp :exports results :results none
 ;; Para html
  (defun filtro-html-misc (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
      números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string ">Nota al pie de p&aacute;gina:"  ">Notas:"
      (replace-regexp-in-string "<p>Última actualización: "  (concat "<p>Última actualización: " (format "%s" (format-time-string "%d/%m/%y")))
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))))
#+end_src

#+ATTR_HTML: :style width:50%
[[./images/manual-emacs.jpg]]

El movimiento por el /software libre/ tal vez sea de esos tramos doctrinales cuya apología
de cara al prójimo resulta casi siempre aridísima, empinada, agria, cuando no una palmaria
quimera que acabará por socavar y corroer el entusiasmo de quien emprende, no sin cierta
ingenuidad, el argumentario. Y es que en cualquier coloquio mundano, y no importa cuán
escorados anden los contertulios hacia todo tema de conversación, ya sea político,
técnico, filosófico, meteorológico o de cualquier palo que se antoje, casar las
computadoras con la libertad individual y la ética lo situará sin remedio a uno en el
incómodo ámbito de lo extemporáneo y lo raro. No importa si la conversación discurre por
términos ---siguiendo la distinción habitual--- profundos o superficiales, si se habla de
Góngora o de marcas de televisores: el golpe de cencerro de quien conjunte el /software/
(esa palabra arcana, extranjera y con su sabor a jerga, que nunca nadie se ha preocupado
por traducir, probablemente por intraducible) con algo tan delicado como la libertad
humana sonará a lo que nunca debe sonar en los oídos sanos, a lo discorde, a lo que chasca
en todo momento inapropiado y fuera de lugar.

Desde luego estamos ante el gran triunfo ---uno más--- del poder. ¿Y dónde está el poder
---palabra tan deslizable y cambiante--- o quién lo ostenta? A decir verdad casi nunca
importa, pues el poder siempre fue el poder, está ahí, desde el primer humano que decidió
aprovecharse de otro. Pero al menos aquí el poder lo vemos escenificado en el ávido
mercado y un mundo gris de oficina siniestra, controlado por las grandes corporaciones. En
manos de éstas se halla todo lo imaginable, incluido el goloso nuevo grial (en realidad ya
no tan nuevo) de la informática y los ordenadores, y con ellos, por añadidura, ese sujeto
que con eufemismo no carente de sinceridad las corporaciones llaman "usuario final":
porque es el último títere que ni siquiera es dueño de los programas que ejecuta.

Toda victoria se nos antoja imposible, eso está claro. Y si alguien consigue siquiera
convencerse de que su libertad está mermada por el /software/ "propietario", y concluye en
que no hace sino apretar botones y resortes en una caja cerrada a cal y canto, sin saber
cuál es la finalidad cierta de esos botones, más allá de dejarle creer que tiene el
control, probablemente mirará a otro lado, resignándose con el triste placebo de que el
porcentaje de libertad que le restan no es, al final del día, tan grande, tratándose como
se trata de computadoras, meros electrodomésticos, cosas que no hay más remedio que usar
en los quehaceres cotidianos, pero que puede uno perfectamente convivir con ellas y su mal
menor y dedicarse a hablar de Góngora por el Whatsapp o por /Zoom/. Sí, es muy típico que
alguien te diga "¡no me hables de ordenadores, que yo no entiendo de eso!", para luego
estar todo el día colgado de aquellos o de otros servicios digitales de dudosa reputación.
Y es que no hay tiranía más efectiva que aquella donde los tiranizados no saben que lo
están. Basta con un cambio, sutil pero inexorable, de paradigma; y hacer creer al vulgo
que las computadoras son, sí, simples electrodomésticos (y no un hito del Humanismo
comparable a la invención de la imprenta); y, en suma, que el /software/ que ejecutan
emana de ellas y no es lo que en el fondo es y nunca dejó de ser: un producto cultural
cuya veda es injusta e inmoral a todas luces. Todo limpio, sin represión ni violencia.

Para hacer frente a tales opresores se necesitan altas dosis de idealismo. E ingenuidad,
sí, ya lo hemos dicho: el no cuidarse de derrotas o victorias, términos esos más propios
del discurso mercantil del contrario, pues no hay revolución más sincera que la que se
sabe destinada al más franco y honrado de los precipicios. Y cabría añadir a lo dicho un
cierto embozo de extravagancia asumida. Richard Matthew Stallman (Nueva York, 1953) no
sólo goza de sobra de esas tres virtudes, sino que fue agraciado también con la de una
rara clarividencia cuando trabajaba en el MIT rodeado de enormes y secretas computadoras
---aquellas máquinas Lisp--- y decidió poner en marcha el movimiento por el /software/
libre en un mundo en que el /software/ no libre difícilmente representaba amenaza alguna
para nadie, y algo como "ordenador personal" sonaba poco más que una contradicción en los
términos. Y es que aquí está el meollo del asunto, y lo asombroso: Stallman decidió
dedicar su vida a la defensa del /software/ libre, promover una fundación, edificar un
sistema operativo cuya licencia de uso no fuese una condena al usuario sino su liberación,
un sistema de todos y de nadie, y todo ello precisamente en un tiempo ---los 80 del pasado
siglo--- cuando el común de los mortales ni usaba computadora ni, mucho menos, la llevaba
como ahora en el bolsillo a guisa de continuidad de su propio cuerpo.

Con su aire desaliñado y hasta anacrónico, sus barbas, su pelo largo, su perfil generoso o
extremado, sus modales en ocasiones chocantes, sobre todo para este siglo puritano,
igualador e imbécil, y ese halo de gurú imprevisto que él mismo se adelantó a todos en
autoparodiar mediante el personaje o /alter ego/ de San Ignucio, Stallman lleva desde
entonces impartiendo conferencias y charlas a lo largo y ancho de medio mundo sobre su
movimiento por el /software/ libre. Y tampoco desde entonces ha cambiado una coma de su
discurso: siempre dice lo mismo, y lo fascinante es que siempre suena a renovado, por obra
de su entusiasmo y su convicción inquebrantables. Su discurso es esencialmente ético, no
técnico, y ya sabemos que lo ético debe volver una vez y otra vez como un golpe de
martillo tenaz: "no matarás", "no robarás", "no mentirás" son preceptos que debemos
repetir cuanto haga falta en su simpleza y transparencia frente a los mercaderes de
matices y los tibios. Ah, los tibios, las grandes víboras... Pero aquí no caben medias
tintas: o usas /software/ libre o usas /software/ propietario. Y debes usar /software/
libre si valoras en algo tu libertad y la de tu comunidad, pues la sola existencia de
/software/ propietario es una injusticia social.

No pocos se han apresurado (incapaces de ver ese entusiasmo y convicción rectoras) a
airear sospechas de que tras su terquedad de una sola dirección y el "no bajarse de la
burra" subyacen en Stallman algunos desórdenes psicológicos. Y en el fondo no deja de ser
irónico el que traten de demostrar vetas autistas en quien más que nadie ha sabido
identificarse con ese maltratado usuario final del que hablábamos. Es digno de notar, en
efecto, que este programador brillantísimo, autor de Emacs, del compilador GCC, impulsor
del sistema operativo GNU, comprenda con una empatía y paciencia admirables al usuario no
versado en intríngulis informáticos (y que ni le interesan, por otra parte), el mismo
usuario que ya está cautivo desde que va a comprar un ordenador a una tienda y se le
impone el impuesto Microsoft o Apple; desde luego lo comprende ciento y mil veces más que
todas las catervas de tecnófilos o tecnópatas que pululan por estas comunidades
"linuxeras" de nuestras entretelas, los que ven la realidad como su propio repositorio de
GitHub, incapaces de trascender la jerga del iniciado. Esos, y no Stallman, son los
verdaderos autistas. Por tanto, no nos engañemos: aquél se dirige (entre otros, pero con
mayor insistencia que a otros) al usuario medio, común y corriente, antes que a los
forofos que cíclicamente van a copar el aforo de sus conferencias y a reírle las gracias
de San Ignucio.

En realidad le habla a todo el que pase por allí. Como un Sócrates moderno, dijo
alguien en Reddit, y no puedo estar más conforme con esta comparación feliz, pues la
afinidad del programador del MIT con el (no menos) extravagante filósofo que vagaba y
divagaba con cualquiera por las estrechas calles de Atenas o por la cabeza ancha de Platón
es patente e inevitable. Por desgracia, como al griego, también le sirven a Stallman la
cicuta de los hipócritas, con la diferencia de que ésta no mata sino que trae el fino
veneno de la muerte pública. Ya dijimos que en la nueva forma de tiranía no es necesaria
la violencia física; a las corporaciones les basta con aventar olas de indignados y
/abajofirmantes/, henchidos de perversidad algunos y tontos útiles los más. Y, entre
medias, ciertas fundaciones de opereta con sus parásitos y paniaguados directivos, que
pretenden hablar por boca de toda una comunidad de usuarios y desarrolladores. Por
supuesto las redes sociales de hogaño, la nueva corte con sus tráfagos y ruidos, donde
confluyen todo vómito y toda escoria y florece la dictadura uniformada de los
bienpensantes, donde cada parroquiano siempre está presto a unirse a una u otra horda y
tomar su piedra arrojadiza, son el amplificador ideal. Y Stallman, ay, la diana perfecta
para cualquier género de vergonzosa manipulación. El discurso de Stallman es simétrico y
claro en su simpleza y resulta por ello un peligro cierto para las corporaciones que tanto
se han beneficiado del /software/ libre. La intención de los mercaderes, nos tememos, no
puede ser otra que la de ejercer el control y despojar la informática de todo componente
ético, especialmente purgando licencias de carácter social, como es la GPL, y promoviendo
otras que les permitan dirigir la orquesta sin tener que devolver a la comunidad lo que la
comunidad ha creado por y para la comunidad. ¿Y quién es, al cabo, la auténtica víctima de
todo eso? Como siempre, el usuario final, que ni siquiera se enterará de estos sucios
movimientos.

Nada nuevo, como ven, pues el mundo no ha dejado de ser el lugar gris e inhóspito que
siempre ha sido. Siento gran tristeza por Stallman, a pesar de que cada vez me cuesta más
conjugar el altruismo del /software/ libre con mi creciente misantropía, máxime asistiendo
al bochornoso espectáculo de parte de mis congéneres. Pero también es cierto que este
texto lo estoy escribiendo en Emacs, parte esencial del proyecto GNU, el cual es de las
pocas cosas netamente idealistas que nos quedan. Toda victoria es imposible, en efecto.
Por eso es tan grato seguir aquí, y comprobar que Stallman continúa, aún con semejante
panorama, incansable en un mundo cansado.

#+begin_export html
<div style="text-align: right;">
<span style="font-size: small;">
#+end_export
Etiquetas: [[file:tag_varia.org][varia]]
#+begin_export html
</span></div>
<div style="text-align: right;">
<span style="font-size: small;"><a href="lista-etiquetas.html">Lista de etiquetas</a></span></div>
#+end_export

* ∞
  :PROPERTIES:
  :CUSTOM_ID: ∞
  :END:

#+begin_export html
<div>
<p>Publicado: 29/03/21 </p>

<p>Última actualización: </p>
</div>
<div>
<hr />
<p>
<a href="diosas-nubes.html">Últimas entradas publicadas</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
