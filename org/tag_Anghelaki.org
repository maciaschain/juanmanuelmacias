#+TITLE: Entradas con la etiqueta: "Anghelaki"

#+SETUPFILE: ~/Git/juanmanuelmacias/html-diosas.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
#+AUTHOR: Juan Manuel Macías
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+OPTIONS: ':t
#+OPTIONS: todo:nil
#+LANGUAGE: es
- [[file:dn_katerina_anghelaki.org][Tres poemas de Katerina Anghelaki Rouke]]  -- 20/11/20
