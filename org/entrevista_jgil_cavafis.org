#+TITLE:Entrevista de Javier Gil en la sección /Adiós Cultural/ de la revista /Adiós/ sobre la /Poesía completa/ de Cavafis
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE:~/Git/gnutas/macros-gnutas.setup
#+SETUPFILE:~/Git/juanmanuelmacias/html.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+OPTIONS: ':t
#+OPTIONS: todo:nil
#+LANGUAGE: es
#+MACRO: img4 @@html:<a style="width:30%;" href="$1"><img src="$2" style="width:20%;"></a>@@



[[./images/regreso-itaca1.jpg]]

[[./images/regreso-itaca2.jpg]]

[[./images/regreso-itaca3.jpg]]
