#+TITLE: Kostas Karyotakis, /Elegías y sátiras y cuatro poemas póstumos/ (Pre-Textos)
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE:~/Git/gnutas/macros-gnutas.setup
#+SETUPFILE:~/Git/juanmanuelmacias/html.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+OPTIONS: ':t
#+OPTIONS: todo:nil
#+LANGUAGE: es


[[./images/karyotakis-banner-pre-textos.jpg]]


*  Menciones, reseñas y ecos

- /Elegías y sátiras y cuatro poemas póstumos/ elegido como el segundo mejor libro de 2018, según los críticos de El
  cultural

  {{{img3(./images/Karyotakis_elcultural2.jpg,./images/Karyotakis_elcultural2.jpg)}}}
